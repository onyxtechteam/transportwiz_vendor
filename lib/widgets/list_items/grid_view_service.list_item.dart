import 'package:flutter/material.dart';
import 'package:transportwiz_vendor/constants/app_colors.dart';
import 'package:transportwiz_vendor/constants/app_strings.dart';
import 'package:transportwiz_vendor/models/service.dart';
import 'package:transportwiz_vendor/utils/ui_spacer.dart';
import 'package:transportwiz_vendor/utils/utils.dart';
import 'package:transportwiz_vendor/widgets/custom_image.view.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:transportwiz_vendor/translations/service.i18n.dart';

class GridViewServiceListItem extends StatelessWidget {
  const GridViewServiceListItem({
    this.service,
    this.onPressed,
    Key key,
  }) : super(key: key);

  final Function(Service) onPressed;
  final Service service;
  @override
  Widget build(BuildContext context) {
    return VStack(
      [
        //service image
        Stack(
          children: [
            Hero(
              tag: service.heroTag,
              child: CustomImage(
                imageUrl:
                    "${service.photos.isNotEmpty ? service?.photos?.first : ''}",
                boxFit: BoxFit.cover,
                width: double.infinity,
                height: Vx.dp64 * 2,
              ),
            ),

            //discount
            Positioned(
              bottom: 0,
              left: Utils.isArabic ? 0 : null,
              right: !Utils.isArabic ? 0 : null,
              child: service.showDiscount
                  ? "${AppStrings.currencySymbol}${service.price - service.discountPrice} ${'Off'.i18n}"
                      .text
                      .white
                      .semiBold
                      .make()
                      .p2()
                      .px4()
                      .box
                      .red500
                      .topRightRounded(value: !Utils.isArabic ? 0 : 10)
                      .topLeftRounded(value: Utils.isArabic ? 0 : 10)
                      .make()
                  : UiSpacer.emptySpace(),
            ),
          ],
        ),

        //
        VStack(
          [
            service.name.text.medium.xl.make(),
            HStack(
              [
                "${AppStrings.currencySymbol}"
                    .text
                    .lg
                    .light
                    .color(AppColor.primaryColor)
                    .make(),
                service.price.text.semiBold
                    .color(AppColor.primaryColor)
                    .xl
                    .make(),
                " ${service.isPerHour ? '/hr' : ''}".text.medium.xl.make(),
              ],
            ),
          ],
        ).p12(),
      ],
    )
        .box
        .withRounded(value: 10)
        .color(context.cardColor)
        .outerShadow
        .clip(Clip.antiAlias)
        .makeCentered()
        .onInkTap(
          () => this.onPressed(this.service),
        );
  }
}
