import 'package:flutter/material.dart';
import 'package:transportwiz_vendor/utils/ui_spacer.dart';
import 'package:transportwiz_vendor/view_models/assign_order.vm.dart';
import 'package:transportwiz_vendor/widgets/buttons/custom_button.dart';
import 'package:transportwiz_vendor/widgets/custom_list_view.dart';
import 'package:transportwiz_vendor/widgets/custom_text_form_field.dart';
import 'package:stacked/stacked.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:transportwiz_vendor/translations/order_details.i18n.dart';

class AssignOrderBottomSheet extends StatelessWidget {
  AssignOrderBottomSheet({Key key, this.onConfirm}) : super(key: key);

  final Function(int) onConfirm;

  //
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<AssignOrderViewModel>.reactive(
      viewModelBuilder: () => AssignOrderViewModel(),
      onModelReady: (vm) => vm.initialise(),
      builder: (context, vm, child) {
        return SafeArea(
          child: VStack(
            [
              //
              "Assign Order To:".i18n.text.semiBold.xl.make(),
              UiSpacer.verticalSpace(),
              CustomTextFormField(
                onChanged: vm.filterDrivers,
              ),
              UiSpacer.verticalSpace(),
              //
              CustomListView(
                isLoading: vm.isBusy,
                dataSet: vm.drivers,
                itemBuilder: (context, index) {
                  //
                  final driver = vm.drivers[index];
                  return HStack(
                    [
                      //
                      Radio(
                        value: driver.id,
                        groupValue: vm.selectedDriverId,
                        onChanged: vm.changeSelectedDriver,
                      ),

                      //
                      "${driver.name}".text.lg.light.make().expand(),
                      //online/offline
                      "[${(driver.isOnline ? "Online".i18n : "Offline".i18n)}]"
                          .text
                          .color(driver.isOnline ? Colors.green : Colors.red)
                          .make(),
                    ],
                  )
                      .onInkTap(() => vm.changeSelectedDriver(driver.id))
                      .wFull(context);
                },
              ).py12().expand(),

              //
              CustomButton(
                title: "Assign".i18n,
                onPressed: () {
                  //
                  onConfirm(vm.selectedDriverId);
                },
              ),
            ],
          ).p20().h(context.safePercentHeight * 80),
        );
      },
    );
  }
}
