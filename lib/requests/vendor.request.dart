import 'package:transportwiz_vendor/constants/api.dart';
import 'package:transportwiz_vendor/models/api_response.dart';
import 'package:transportwiz_vendor/models/vendor.dart';
import 'package:transportwiz_vendor/services/auth.service.dart';
import 'package:transportwiz_vendor/services/http.service.dart';

class VendorRequest extends HttpService {
  //
  Future<dynamic> getVendorDetails() async {
    final vendorId = (await AuthServices.getCurrentUser(force: true)).vendor_id;
    final apiResult = await get(
      Api.vendorDetails.replaceFirst("id", vendorId.toString()),
    );

    //
    final apiResponse = ApiResponse.fromResponse(apiResult);
    if (apiResponse.allGood) {
      return apiResponse.body;
    } else {
      throw apiResponse.message;
    }
  }

  Future<ApiResponse> toggleVendorAvailablity(Vendor vendor) async {
    final apiResult = await post(
      Api.vendorAvailability.replaceFirst(
        "id",
        vendor.id.toString(),
      ),
      {
        "is_open": !vendor.isOpen,
      },
    );
    return ApiResponse.fromResponse(apiResult);
  }
}
