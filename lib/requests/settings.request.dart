import 'package:transportwiz_vendor/constants/api.dart';
import 'package:transportwiz_vendor/models/api_response.dart';
import 'package:transportwiz_vendor/services/http.service.dart';

class SettingsRequest extends HttpService {
  //
  Future<ApiResponse> appSettings() async {
    final apiResult = await get(
      Api.appSettings
    );
    return ApiResponse.fromResponse(apiResult);
  }

}
