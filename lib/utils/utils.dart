import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:transportwiz_vendor/models/vendor.dart';
import 'package:transportwiz_vendor/views/pages/package_types/package_type_pricing.page.dart';
import 'package:transportwiz_vendor/views/pages/product/products.page.dart';
import 'package:transportwiz_vendor/views/pages/service/service.page.dart';
import 'package:i18n_extension/i18n_widget.dart';

class Utils {

  //
  static bool get isArabic => I18n.language == "ar";
  //
  static IconData vendorIconIndicator(Vendor vendor) {
    return ((vendor == null || (!vendor.isPackageType && !vendor.isServiceType))
        ? FlutterIcons.archive_fea
        : vendor.isServiceType
            ? FlutterIcons.rss_fea
            : FlutterIcons.money_faw);
  }

  //
  static String vendorTypeIndicator(Vendor vendor) {
    return ((vendor == null || (!vendor.isPackageType && !vendor.isServiceType))
        ? 'Products'
        : vendor.isServiceType
            ? "Services"
            : 'Pricing');
  }

  static Widget vendorSectionPage(Vendor vendor) {
    return ((vendor == null || (!vendor.isPackageType && !vendor.isServiceType))
        ? ProductsPage()
        : vendor.isServiceType
            ? ServicePage()
            : PackagePricingPage());
  }
}
