import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:transportwiz_vendor/constants/app_strings.dart';
import 'package:transportwiz_vendor/models/delivery_address.dart';
import 'package:transportwiz_vendor/views/pages/shared/custom_webview.page.dart';
import 'package:stacked/stacked.dart';
import 'package:firestore_repository/firestore_repository.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:velocity_x/velocity_x.dart';

class MyBaseViewModel extends BaseViewModel {
  //
  BuildContext viewContext;
  final formKey = GlobalKey<FormState>();
  final formBuilderKey = GlobalKey<FormBuilderState>();
  final currencySymbol = AppStrings.currencySymbol;
  DeliveryAddress deliveryaddress = DeliveryAddress();
  String firebaseVerificationId;

  void initialise() {
    FirestoreRepository();
  }

  openWebpageLink(String url) async {
    //
    if (Platform.isIOS) {
      await launch(url);
      return;
    }
    await viewContext.push(
      (context) => CustomWebviewPage(
        selectedUrl: url,
      ),
    );
  }
}
