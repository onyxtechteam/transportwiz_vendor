import 'dart:io';
import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';
import 'package:transportwiz_vendor/models/product_category.dart';
import 'package:transportwiz_vendor/models/service.dart';
import 'package:transportwiz_vendor/requests/product.request.dart';
import 'package:transportwiz_vendor/requests/service.request.dart';
import 'package:transportwiz_vendor/requests/vendor.request.dart';
import 'package:transportwiz_vendor/services/auth.service.dart';
import 'package:transportwiz_vendor/view_models/base.view_model.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:transportwiz_vendor/translations/product.i18n.dart';

class EditServiceViewModel extends MyBaseViewModel {
  //
  EditServiceViewModel(BuildContext context, this.service) {
    this.viewContext = context;
  }

  //
  ServiceRequest serviceRequest = ServiceRequest();
  ProductRequest productRequest = ProductRequest();
  VendorRequest vendorRequest = VendorRequest();
  Service service;
  List<ProductCategory> categories = [];
  List<File> selectedPhotos;

  void initialise() {
    fetchVendorTypeCategories();
  }

  //
  fetchVendorTypeCategories() async {
    setBusyForObject(categories, true);

    try {
      categories = await productRequest.getProductCategories(
        vendorTypeId:
            (await AuthServices.getCurrentVendor(force: true)).vendorType.id,
      );
      clearErrors();
    } catch (error) {
      print("Categories Error ==> $error");
      setError(error);
    }

    setBusyForObject(categories, false);
  }

  //
  onImagesSelected(List<File> files) {
    selectedPhotos = files;
    notifyListeners();
  }

  //
  processUpdateService() async {
    if (formBuilderKey.currentState.saveAndValidate() &&
        validateSelectedPhotos()) {
      //
      setBusy(true);

      try {
        final apiResponse = await serviceRequest.updateService(
          service,
          data: formBuilderKey.currentState.value,
          photos: selectedPhotos,
        );
        //
        if (apiResponse.allGood) {
          service = Service.fromJson(apiResponse.body["service"]);
        }
        //show dialog to present state
        CoolAlert.show(
            context: viewContext,
            type: apiResponse.allGood
                ? CoolAlertType.success
                : CoolAlertType.error,
            title: "Update Service".i18n,
            text: apiResponse.message,
            onConfirmBtnTap: () {
              viewContext.pop();
              if (apiResponse.allGood) {
                viewContext.pop(service);
              }
            });
        clearErrors();
      } catch (error) {
        print("Update service Error ==> $error");
        setError(error);
      }

      setBusy(false);
    }
  }

  bool validateSelectedPhotos() {
    if (selectedPhotos == null || selectedPhotos.isEmpty) {
      CoolAlert.show(
        context: viewContext,
        type:CoolAlertType.warning,
        title: "Update Service".i18n,
        text: "Please select at least one photo for service".i18n,
      );
      return false;
    }
    return true;
  }
}
