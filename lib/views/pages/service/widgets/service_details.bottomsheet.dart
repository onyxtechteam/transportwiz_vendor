import 'package:flutter/material.dart';
import 'package:transportwiz_vendor/utils/ui_spacer.dart';
import 'package:transportwiz_vendor/view_models/service_details.vm.dart';
import 'package:transportwiz_vendor/widgets/buttons/custom_button.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:transportwiz_vendor/translations/service.i18n.dart';

class ServiceDetailsBottomSheet extends StatelessWidget {
  const ServiceDetailsBottomSheet(this.vm, {Key key}) : super(key: key);
  final ServiceDetailsViewModel vm;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: HStack(
        [
          //
          CustomButton(
            title: "Edit".i18n,
            color: Colors.grey,
            loading: vm.isBusy,
            onPressed: vm.editService,
          ).expand(),
          UiSpacer.horizontalSpace(),
          //
          CustomButton(
            title: "Delete".i18n,
            color: Colors.red,
            loading: vm.isBusy,
            onPressed: vm.deleteService,
          ).expand(),
        ],
      ).p20(),
    ).box.shadowSm.color(context.backgroundColor).topRounded(value: 20).make();
  }
}
