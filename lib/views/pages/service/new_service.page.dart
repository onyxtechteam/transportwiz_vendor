import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:transportwiz_vendor/utils/ui_spacer.dart';
import 'package:transportwiz_vendor/view_models/new_service.vm.dart';
import 'package:transportwiz_vendor/widgets/base.page.dart';
import 'package:transportwiz_vendor/widgets/busy_indicator.dart';
import 'package:transportwiz_vendor/widgets/buttons/custom_button.dart';
import 'package:transportwiz_vendor/widgets/cards/multiple_image_selector.dart';
import 'package:stacked/stacked.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:transportwiz_vendor/translations/service.i18n.dart';

class NewServicePage extends StatelessWidget {
  const NewServicePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<NewServiceViewModel>.reactive(
      viewModelBuilder: () => NewServiceViewModel(context),
      onModelReady: (vm) => vm.initialise(),
      builder: (context, vm, child) {
        return BasePage(
          showAppBar: true,
          showLeadingAction: true,
          title: "New Service".i18n,
          body: FormBuilder(
            key: vm.formBuilderKey,
            child: VStack(
              [
                //categories
                vm.busy(vm.categories)
                    ? BusyIndicator().centered()
                    : VStack(
                        [
                          "Category".i18n.text.make(),
                          FormBuilderDropdown(
                            name: "category_id",
                            items: vm.categories
                                .map(
                                  (category) => DropdownMenuItem(
                                    value: '${category.id}',
                                    child: Text('${category.name}'),
                                  ),
                                )
                                .toList(),
                            validator: FormBuilderValidators.compose([
                              FormBuilderValidators.required(context),
                            ]),
                          ),
                        ],
                      ),

                UiSpacer.verticalSpace(),

                //name
                FormBuilderTextField(
                  name: 'name',
                  decoration: InputDecoration(
                    labelText: 'Name'.i18n,
                  ),
                  onChanged: (value) {},
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(context),
                  ]),
                ),
                UiSpacer.verticalSpace(),
                //description
                FormBuilderTextField(
                  name: 'description',
                  decoration: InputDecoration(
                    labelText: 'Description'.i18n,
                  ),
                  onChanged: (value) {},
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(context),
                  ]),
                  maxLines: 3,
                  textInputAction: TextInputAction.newline,
                ),
                UiSpacer.verticalSpace(),
                //image
                MultipleImageSelectorView(
                  onImagesSelected: vm.onImagesSelected,
                ),
                UiSpacer.verticalSpace(),
                //pricing
                HStack(
                  [
                    //price
                    FormBuilderTextField(
                      name: 'price',
                      decoration: InputDecoration(
                        labelText: 'Price'.i18n,
                      ),
                      onChanged: (value) {},
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(context),
                        FormBuilderValidators.numeric(context),
                      ]),
                      keyboardType: TextInputType.numberWithOptions(
                        decimal: true,
                        signed: true,
                      ),
                    ).expand(),
                    UiSpacer.horizontalSpace(),
                    //Discount price
                    FormBuilderTextField(
                      name: 'discount_price',
                      decoration: InputDecoration(
                        labelText: 'Discount Price'.i18n,
                      ),
                      onChanged: (value) {},
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(context),
                        FormBuilderValidators.numeric(context),
                      ]),
                      keyboardType: TextInputType.numberWithOptions(
                        decimal: true,
                        signed: true,
                      ),
                    ).expand(),
                  ],
                ),

                //checkbox
                HStack(
                  [
                    //Per hour
                    FormBuilderCheckbox(
                      initialValue: false,
                      name: 'per_hour',
                      onChanged: (value) {},
                      valueTransformer: (value) => value ? 1 : 0,
                      title: "Per Hour".i18n.text.make(),
                    ).expand(),
                    //Active
                    FormBuilderCheckbox(
                      initialValue: true,
                      name: 'is_active',
                      onChanged: (value) {},
                      valueTransformer: (value) => value ? 1 : 0,
                      title: "Active".i18n.text.make(),
                    ).expand(),
                  ],
                ),
                //

                //
                CustomButton(
                  title: "Save".i18n,
                  icon: FlutterIcons.save_fea,
                  loading: vm.isBusy,
                  onPressed: vm.processNewService,
                ).wFull(context).py12(),
                UiSpacer.verticalSpace(),
                UiSpacer.verticalSpace(),
              ],
            )
                .p20()
                .scrollVertical()
                .pOnly(bottom: context.mq.viewInsets.bottom),
          ),
        );
      },
    );
  }
}
