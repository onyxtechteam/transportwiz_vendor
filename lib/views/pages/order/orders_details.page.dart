import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:transportwiz_vendor/constants/app_colors.dart';
import 'package:transportwiz_vendor/models/order.dart';
import 'package:transportwiz_vendor/constants/app_strings.dart';
import 'package:transportwiz_vendor/utils/ui_spacer.dart';
import 'package:transportwiz_vendor/view_models/order_details.vm.dart';
import 'package:transportwiz_vendor/views/pages/order/widgets/order_actions.dart';
import 'package:transportwiz_vendor/views/pages/order/widgets/order_address.view.dart';
import 'package:transportwiz_vendor/views/pages/order/widgets/order_details_items.view.dart';
import 'package:transportwiz_vendor/views/pages/order/widgets/order_status.view.dart';
import 'package:transportwiz_vendor/widgets/base.page.dart';
import 'package:transportwiz_vendor/widgets/busy_indicator.dart';
import 'package:transportwiz_vendor/widgets/buttons/custom_button.dart';
import 'package:transportwiz_vendor/widgets/cards/order_summary.dart';
import 'package:stacked/stacked.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:transportwiz_vendor/translations/order_details.i18n.dart';

class OrderDetailsPage extends StatelessWidget {
  const OrderDetailsPage({this.order, Key key}) : super(key: key);

  //
  final Order order;

  @override
  Widget build(BuildContext context) {
    //
    return ViewModelBuilder<OrderDetailsViewModel>.reactive(
      viewModelBuilder: () => OrderDetailsViewModel(context, order),
      onModelReady: (vm) => vm.initialise(),
      builder: (context, vm, child) {
        return BasePage(
          title: "Order Details".i18n,
          showAppBar: true,
          showLeadingAction: true,
          onBackPressed: vm.onBackPressed,
          isLoading: vm.isBusy,
          body: vm.isBusy
              ? BusyIndicator().centered()
              : VStack(
                  [
                    //code & total amount
                    HStack(
                      [
                        //
                        VStack(
                          [
                            "Code".i18n.text.gray500.medium.sm.make(),
                            "#${vm.order.code}".text.medium.xl.make(),
                          ],
                        ).expand(),
                        //total amount
                        AppStrings.currencySymbol.text.medium.lg.make().px4(),
                        (vm.order.total ?? 0.00)
                            .numCurrency
                            .text
                            .medium
                            .xl2
                            .make(),
                      ],
                    ).pOnly(bottom: Vx.dp20),

                    //show package delivery addresses
                    OrderAddressView(vm),

                    //status
                    OrderStatusView(vm),

                    //Payment status
                    VStack(
                      [
                        //
                        "Payment Status".i18n.text.gray500.medium.sm.make(),
                        //
                        ((vm.order.paymentStatus ?? "")
                                    .i18n
                                    .allWordsCapitilize() ??
                                "")
                            .text
                            .color(
                                AppColor.getStausColor(vm.order.paymentStatus))
                            .medium
                            .xl
                            .make(),
                        //
                      ],
                    ).pOnly(bottom: Vx.dp20),

                    //driver
                    vm.order.driver != null
                        ? HStack(
                            [
                              //
                              VStack(
                                [
                                  "Driver".i18n.text.gray500.medium.sm.make(),
                                  (vm.order.driver.name.allWordsCapitilize() ??
                                          vm.order.driver.name ??
                                          "")
                                      .text
                                      .medium
                                      .xl
                                      .make()
                                      .pOnly(bottom: Vx.dp20),
                                ],
                              ).expand(),
                              //call
                              CustomButton(
                                icon: FlutterIcons.phone_call_fea,
                                iconColor: Colors.white,
                                color: AppColor.primaryColor,
                                shapeRadius: Vx.dp20,
                                onPressed: vm.callDriver,
                              ).wh(Vx.dp64, Vx.dp40).p12(),
                            ],
                          )
                        : UiSpacer.emptySpace(),

                    //chat
                    vm.order.canChatDriver
                        ? CustomButton(
                            icon: FlutterIcons.chat_ent,
                            iconColor: Colors.white,
                            title: "Chat with driver".i18n,
                            color: AppColor.primaryColor,
                            onPressed: vm.chatDriver,
                          ).h(Vx.dp48).pOnly(top: Vx.dp12, bottom: Vx.dp20)
                        : UiSpacer.emptySpace(),

                    //customer
                    HStack(
                      [
                        //
                        VStack(
                          [
                            "Customer".i18n.text.gray500.medium.sm.make(),
                            vm.order.user.name
                                .allWordsCapitilize()
                                .text
                                .medium
                                .xl
                                .make()
                                .pOnly(bottom: Vx.dp20),
                          ],
                        ).expand(),
                        //call
                        vm.order.canChatCustomer
                            ? CustomButton(
                                icon: FlutterIcons.phone_call_fea,
                                iconColor: Colors.white,
                                color: AppColor.primaryColor,
                                shapeRadius: Vx.dp20,
                                onPressed: vm.callCustomer,
                              ).wh(Vx.dp64, Vx.dp40).p12()
                            : UiSpacer.emptySpace(),
                      ],
                    ),
                    vm.order.canChatCustomer
                        ? CustomButton(
                            icon: FlutterIcons.chat_ent,
                            iconColor: Colors.white,
                            title: "Chat with customer".i18n,
                            color: AppColor.primaryColor,
                            onPressed: vm.chatCustomer,
                          ).h(Vx.dp48).pOnly(top: Vx.dp12, bottom: Vx.dp20)
                        : UiSpacer.emptySpace(),

                    //recipient
                    // RecipientInfo(
                    //   callRecipient: vm.callRecipient,
                    //   order: vm.order,
                    // ),

                    //note
                    "Note".i18n.text.gray500.medium.sm.make(),
                    (vm.order.note ?? "--")
                        .text
                        .medium
                        .xl
                        .italic
                        .make()
                        .pOnly(bottom: Vx.dp20),

                    // either products/package details
                    OrderDetailsItemsView(vm),

                    //order summary
                    OrderSummary(
                      subTotal: vm.order.subTotal,
                      driverTrip: vm.order.tip,
                      discount: vm.order.discount,
                      deliveryFee: vm.order.deliveryFee,
                      tax: vm.order.tax,
                      vendorTax: ((vm.order.tax / vm.order.total) * 100)
                          .toDoubleStringAsFixed(),
                      total: vm.order.total,
                    ).pOnly(top: Vx.dp20, bottom: Vx.dp56),
                  ],
                )
                  .p20()
                  .pOnly(bottom: context.percentHeight * 30)
                  .scrollVertical(),

          //fab printing
          fab: Platform.isAndroid
              ? FloatingActionButton.extended(
                  onPressed: vm.printOrder,
                  label: "Print".text.white.make(),
                  backgroundColor: AppColor.primaryColor,
                  icon: Icon(
                    FlutterIcons.print_faw,
                    color: Colors.white,
                  ),
                )
              : null,
          //bottomsheet
          bottomSheet: vm.order.canShowActions
              ? OrderActions(
                  order: vm.order,
                  canChatCustomer: vm.order.canChatCustomer,
                  busy: vm.isBusy || vm.busy(vm.order),
                  onEditPressed: vm.changeOrderStatus,
                  onAssignPressed: vm.assignOrder,
                  onCancelledPressed: vm.processOrderCancellation,
                )
              : null,
        );
      },
    );
  }
}
