import 'package:flutter/material.dart';
import 'package:transportwiz_vendor/utils/ui_spacer.dart';
import 'package:transportwiz_vendor/view_models/order_details.vm.dart';
import 'package:transportwiz_vendor/widgets/cards/amount_tile.dart';
import 'package:transportwiz_vendor/widgets/custom_image.view.dart';
import 'package:transportwiz_vendor/widgets/custom_list_view.dart';
import 'package:transportwiz_vendor/widgets/list_items/order_product.list_item.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:transportwiz_vendor/translations/order_details.i18n.dart';

class OrderDetailsItemsView extends StatelessWidget {
  const OrderDetailsItemsView(this.vm, {Key key}) : super(key: key);
  final OrderDetailsViewModel vm;

  @override
  Widget build(BuildContext context) {
    return VStack(
      [
        (vm.order.isPackageDelivery
                ? "Package Details"
                : vm.order.isSerice
                    ? "Service"
                    : "Products")
            .i18n
            .text
            .gray500
            .semiBold
            .xl
            .make()
            .pOnly(bottom: Vx.dp10),
        vm.order.isPackageDelivery
            ? VStack(
                [
                  AmountTile(
                    "Package Type".i18n,
                    vm.order.packageType.name,
                  ),
                  AmountTile("Width".i18n, vm.order.width + "cm"),
                  AmountTile("Length".i18n, vm.order.length + "cm"),
                  AmountTile("Height".i18n, vm.order.height + "cm"),
                  AmountTile("Weight".i18n, vm.order.weight + "kg"),
                ],
                crossAlignment: CrossAxisAlignment.end,
              )
            : vm.order.isSerice
                ? VStack(
                    [
                      AmountTile(
                        "Service".i18n,
                        vm.order.orderService.service.name,
                      ),
                      AmountTile(
                        "Category".i18n,
                        vm.order.orderService.service.category.name,
                      ),
                    ],
                    crossAlignment: CrossAxisAlignment.end,
                  )
                : CustomListView(
                    noScrollPhysics: true,
                    dataSet: vm.order.orderProducts,
                    itemBuilder: (context, index) {
                      //
                      final orderProduct = vm.order.orderProducts[index];
                      return OrderProductListItem(
                        orderProduct: orderProduct,
                      );
                    },
                  ),

        //order photo
        (vm.order.photo != null && !vm.order.photo.contains("default.png"))
            ? CustomImage(
                imageUrl: vm.order.photo,
                boxFit: BoxFit.fill,
              ).h(context.percentHeight * 30).wFull(context)
            : UiSpacer.emptySpace(),
      ],
    );
  }
}
