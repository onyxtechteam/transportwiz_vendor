import 'package:double_back_to_close/double_back_to_close.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:transportwiz_vendor/utils/utils.dart';
import 'package:transportwiz_vendor/views/pages/profile/profile.page.dart';
import 'package:transportwiz_vendor/view_models/home.vm.dart';
import 'package:transportwiz_vendor/views/pages/vendor/vendor_details.page.dart';
import 'package:transportwiz_vendor/widgets/base.page.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:stacked/stacked.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:transportwiz_vendor/translations/home.i18n.dart';

import 'order/orders.page.dart';

class HomePage extends StatefulWidget {
  HomePage({
    Key key,
  }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return DoubleBack(
      message: "Press back again to close".i18n,
      child: ViewModelBuilder<HomeViewModel>.reactive(
        viewModelBuilder: () => HomeViewModel(context),
        onModelReady: (model) => model.initialise(),
        builder: (context, model, child) {
          return BasePage(
            body: PageView(
              controller: model.pageViewController,
              onPageChanged: model.onPageChanged,
              children: [
                OrdersPage(),
                //
                Utils.vendorSectionPage(model.currentVendor),

                VendorDetailsPage(),
                ProfilePage(),
              ],
            ),
            bottomNavigationBar: VxBox(
              child: SafeArea(
                child: GNav(
                  gap: 8,
                  activeColor: Colors.white,
                  color: Theme.of(context).textTheme.bodyText1.color,
                  iconSize: 20,
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  duration: Duration(milliseconds: 250),
                  tabBackgroundColor: Theme.of(context).accentColor,
                  tabs: [
                    GButton(
                      icon: FlutterIcons.inbox_ant,
                      text: 'Orders'.i18n,
                    ),
                    GButton(
                      icon: Utils.vendorIconIndicator(model.currentVendor),
                      text: Utils.vendorTypeIndicator(model.currentVendor).i18n,
                    ),
                    GButton(
                      icon: FlutterIcons.briefcase_fea,
                      text: 'Vendor'.i18n,
                    ),
                    GButton(
                      icon: FlutterIcons.menu_fea,
                      text: 'More'.i18n,
                    ),
                  ],
                  selectedIndex: model.currentIndex,
                  onTabChange: model.onTabChange,
                ),
              ),
            )
                .p16
                .shadow
                .color(Theme.of(context).bottomSheetTheme.backgroundColor)
                .make(),
          );
        },
      ),
    );
  }
}
